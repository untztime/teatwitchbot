// Gets last line of .m3u file and takes file leaf. m3u file based on date format for VirtualDJ
using System.Text.RegularExpressions;

static class PlayingAtm
{
    public static Task<string> GetPlayingAtm(string m3uLocation)
    {
        var artistRegex = new Regex("<artist>(.*?)</artist>");
        var titleRegex = new Regex("<title>(.*?)</title>");        
        
        string formatDate = DateTime.Now.ToString("yyyy-MM-dd");
        string path = m3uLocation + formatDate + ".m3u";

        string[] lines = System.IO.File.ReadAllLines(path);
        string secondLastLine = lines[lines.Length - 2];

        // Find the matches in the second last line
        var artistMatch = artistRegex.Match(secondLastLine);
        var titleMatch = titleRegex.Match(secondLastLine);

        if (artistMatch.Success && titleMatch.Success)
        {
            // Extract the artist and title
            var artist = artistMatch.Groups[1].Value;
            var title = titleMatch.Groups[1].Value;

            return Task.FromResult($"{artist} - {title}");
        }


        return Task.FromResult("No match found");
    }


}