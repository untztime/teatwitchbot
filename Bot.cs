using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;
using TwitchLib.Communication.Clients;
using TwitchLib.Communication.Models;

public class Bot
{
    TwitchClient client;
    FileSystemWatcher? watcher;
    public string ChannelName { get; set; } = "";
    public string M3ULocation { get; set; } = "";

    public Bot(string accessToken, string channelName, string m3uLocation)
    {
        ChannelName = channelName;
        M3ULocation = m3uLocation;

        Console.WriteLine("Bot created");
        ConnectionCredentials credentials = new ConnectionCredentials("TmTwtBot", accessToken);
        var clientOptions = new ClientOptions
        {
                MessagesAllowedInPeriod = 750,
                ThrottlingPeriod = TimeSpan.FromSeconds(30)
        };
        WebSocketClient customClient = new WebSocketClient(clientOptions);
        client = new TwitchClient(customClient);
        client.Initialize(credentials, channelName);

        client.OnLog += Client_OnLog;
        client.OnJoinedChannel += Client_OnJoinedChannel;
        client.OnMessageReceived += Client_OnMessageReceived;
        client.OnConnected += Client_OnConnected;

        WatchFile();

        client.Connect();
    }

    public void WatchFile()
    {
        string formatDate = DateTime.Now.ToString("yyyy-MM-dd");
        string path = M3ULocation + formatDate + ".m3u";
        watcher = new FileSystemWatcher()
        {
            Path = M3ULocation,
            NotifyFilter = NotifyFilters.LastWrite,
            Filter = "*.m3u",
        };
        watcher.Changed += OnChanged;
        watcher.EnableRaisingEvents = true;
    }


    private void OnChanged(object source, FileSystemEventArgs e)
    {
        string nowPlaying = PlayingAtm.GetPlayingAtm(M3ULocation).Result;
        client.SendMessage(ChannelName, nowPlaying);
    }

    private void Client_OnLog(object sender, OnLogArgs e)
    {
        Console.WriteLine($"{e.DateTime.ToString()}: {e.BotUsername} - {e.Data}");
    }

    private void Client_OnConnected(object sender, OnConnectedArgs e)
    {
        Console.WriteLine($"Connected to {e.AutoJoinChannel}");
    }

    private void Client_OnJoinedChannel(object sender, OnJoinedChannelArgs e)
    {
        client.SendMessage(e.Channel, "Bot started!");
    }

    private void Client_OnMessageReceived(object sender, OnMessageReceivedArgs e)
    {
        if (e.ChatMessage.Message.Contains("id?"))
        {
            string nowPlaying = PlayingAtm.GetPlayingAtm(M3ULocation).Result;
            client.SendMessage(e.ChatMessage.Channel, nowPlaying);
        }
    }
}