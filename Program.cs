using System.Text.Json;
using System.Diagnostics;

TwitchAccessToken accessToken = new TwitchAccessToken();

var config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: false)
    .Build();

string clientId = config["clientId"];
string clientSecret = config["clientSecret"];
string channelName = config["channelName"];

string m3uLocation = File.ReadAllText("m3uLocation.txt");

Console.WriteLine("Do you need an access token? type y or your token if you have one");
var response = Console.ReadLine();


if (response == "y") {
    string url = "https://id.twitch.tv/oauth2/authorize?client_id="+clientId+"&redirect_uri=http://localhost:5241/token&response_type=code&scope=chat:read+chat:edit";
    string escapedUrl = System.Uri.EscapeUriString(url);
    Process.Start(new ProcessStartInfo("cmd", $"/c start \"\" \"{escapedUrl}\"") { CreateNoWindow = true });
    Console.WriteLine("Please find access_token in browser, then restart the program");
}

if (response == "" || response == null) {
    throw new Exception("Access token cannot be empty");
}

if (response != "y") {
    accessToken.access_token = response;
}

var builder = WebApplication.CreateBuilder(args);
if (!String.IsNullOrEmpty(accessToken.access_token) && !String.IsNullOrEmpty(channelName)) {
    builder.Services.AddSingleton<Bot>(new Bot(accessToken.access_token, channelName, m3uLocation));
}

var app = builder.Build();

app.MapGet("/token", async (HttpContext context) =>
{
    var code = context.Request.Query["code"];
    var client = new HttpClient();
    var response = await client.PostAsync($"https://id.twitch.tv/oauth2/token?client_id={clientId}&client_secret={clientSecret}&code=" + code + "&grant_type=authorization_code&redirect_uri=http://localhost:5241/token", null);
    var json = await response.Content.ReadAsStringAsync();
    if (response.IsSuccessStatusCode && json != null) {
        accessToken = JsonSerializer.Deserialize<TwitchAccessToken>(json);
    }

    return json;
});

app.Run();
